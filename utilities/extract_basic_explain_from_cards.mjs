/*
* for i in $(seq -w 8105); do node main.mjs ./cards/$i.html > ./basic_explains/$i.html; done
*/

import { parse } from 'node-html-parser';
import { readFileSync } from 'node:fs';
import { argv } from 'node:process';

const document= parse(readFileSync(argv[2]));
var node = document.querySelector("#ziDetails > div > div.zi-basic-explain")
if (null != node){
    console.log(node.outerHTML);
}else{
    console.log("NAN");
}
