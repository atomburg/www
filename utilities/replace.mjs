import { parse } from 'node-html-parser';
import { readFileSync } from 'node:fs';
import { writeFileSync } from 'node:fs';
import { argv } from 'node:process';

var NUMBER_BEGIN = 1;
var NUMBER_END = 8105;

function padNumber(num, length) {
    let numStr = num.toString();
    while (numStr.length < length) {
        numStr = '0' + numStr;
    }
    return numStr;
}

var doc = parse(readFileSync("./hanzi.html"));
for (var i = NUMBER_BEGIN; i <= NUMBER_END; i++){
	console.log(padNumber(i,4));
	var basic_explain = readFileSync("./basic_explain_htmls/" + padNumber(i, 4) + ".html");
	var node = doc.querySelector("#basic_explain_"+padNumber(i, 4))
	node.innerHTML = basic_explain;
}

writeFileSync("./out.html", doc.toString());
