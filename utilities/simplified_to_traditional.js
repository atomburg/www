const OpenCC = require('opencc');
const FS = require('fs');
const process = require('process');
const converter = new OpenCC('s2t.json');
var src = FS.readFileSync(process.argv[2], 'utf-8');
converter.convertPromise(src).then(converted => {
  console.log(converted);  // 漢字
});