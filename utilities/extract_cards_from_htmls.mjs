/*
* for i in $(seq -w 8105); do node main.mjs ./htmls/$i.html > ./cards/$i.html; done
*/

import { parse } from 'node-html-parser';
import { readFileSync } from 'node:fs';
import { argv } from 'node:process';

const document= parse(readFileSync(argv[2]));
var node = document.querySelector("body > div.container > div > div.col.main-content > div:nth-child(4)")
if (null == node) node = document.querySelector("body > div.container > div > div.col.main-content > div:nth-child(3)")
console.log(node.toString());
