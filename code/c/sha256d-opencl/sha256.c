#include "sha256.h"

int sha256d(char* input, size_t string_len, char* output)
{
	cl_int ret;
 	cl_platform_id platform_id = NULL;
	cl_device_id device_id = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_context context;

	cl_program program;
	cl_kernel kernel;
	cl_command_queue command_queue;

	cl_mem  buffer_out, buffer_keys;
	unsigned int hashes[SHA256_RESULT_SIZE];

	size_t global_work_size[3] = { 1,1,1 };
	size_t local_work_size[3] = { 1,1,1 };

	char* source_str;
	size_t source_size;
	FILE* fp = fopen("sha256.cl", "r");
	if (!fp) {
		fprintf(stderr, "Failed to load kernel.\n");
		exit(1);
	}
	source_str = (char*)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);

	ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ALL, 1, &device_id, &ret_num_devices);
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

	program = clCreateProgramWithSource(context, 1, (const char**)&source_str, (const size_t*)&source_size, &ret);
	ret = clBuildProgram(program, 1, &device_id, "-Werror -DMAX_MESSAGE_SIZE=128 -DSHA256_RESULT_SIZE=8", NULL, NULL);
	kernel = clCreateKernel(program, "sha256D", &ret);
	
	command_queue = clCreateCommandQueue(context, device_id, 0, &ret);

	ret = clSetKernelArg(kernel, 0, sizeof(string_len), (void*)&string_len);

	buffer_keys = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_uchar) * (MAX_MESSAGE_SIZE), NULL, &ret);
	ret = clSetKernelArg(kernel, 1, sizeof(buffer_keys), (void*)&buffer_keys);

	buffer_out = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_uint) * SHA256_RESULT_SIZE, NULL, &ret);
	ret = clSetKernelArg(kernel, 2, sizeof(buffer_out), (void*)&buffer_out);

	ret = clEnqueueWriteBuffer(command_queue, buffer_keys, CL_TRUE, 0, string_len, input, 0, NULL, NULL);
	ret = clEnqueueNDRangeKernel(command_queue, kernel, 3, NULL, global_work_size, local_work_size, 0, NULL, NULL);
	ret = clFinish(command_queue);
	ret = clEnqueueReadBuffer(command_queue, buffer_out, CL_TRUE, 0, sizeof(cl_uint) * SHA256_RESULT_SIZE, hashes, 0, NULL, NULL);

	for(int i = 0; i<SHA256_RESULT_SIZE; i++)
	{
		sprintf(output+i*8,"%08x", hashes[i]);
	}
	return ret;
}
