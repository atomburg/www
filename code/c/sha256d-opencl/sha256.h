#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_MESSAGE_SIZE    		128

#define SHA256_RESULT_SIZE			8

#define MAX_SOURCE_SIZE             0x10000000

#ifdef __cplusplus
extern "C" {
#endif

int sha256d(char*, size_t, char*);

#ifdef __cplusplus
}
#endif