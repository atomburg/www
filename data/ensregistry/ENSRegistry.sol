/**
 *Submitted for verification at Etherscan.io on 2020-01-30
*/

// SPDX-License-Identifier: MIT

// File: @ensdomains/ens/contracts/ENS.sol

pragma solidity ^0.8.26;

interface ENS {

    // Logged when the owner of a node assigns a new owner to a subnode.
    event NewOwner(bytes32 indexed node, bytes32 indexed label, address owner);

    // Logged when the owner of a node transfers ownership to a new account.
    event Transfer(bytes32 indexed node, address owner);

    // Logged when the resolver for a node changes.
    event NewResolver(bytes32 indexed node, address resolver);

    // Logged when the TTL of a node changes
    event NewTTL(bytes32 indexed node, uint64 ttl);

    // Logged when an operator is added or removed.
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);

    function setRecord(bytes32 node, address owner, address resolver, uint64 ttl) external;
    function setSubnodeRecord(bytes32 node, bytes32 label, address owner, address resolver, uint64 ttl) external;
    function setSubnodeOwner(bytes32 node, bytes32 label, address owner) external returns(bytes32);
    function setResolver(bytes32 node, address resolver) external;
    function setOwner(bytes32 node, address owner) external;
    function setTTL(bytes32 node, uint64 ttl) external;
    function setApprovalForAll(address operator, bool approved) external;
    function owner(bytes32 node) external view returns (address);
    function resolver(bytes32 node) external view returns (address);
    function ttl(bytes32 node) external view returns (uint64);
    function recordExists(bytes32 node) external view returns (bool);
    function isApprovedForAll(address owner, address operator) external view returns (bool);
}

// File: @ensdomains/ens/contracts/ENSRegistry.sol

/**
 * The ENS registry contract.
 */
contract ENSRegistry is ENS {

    struct Record {
        address owner;
        address resolver;
        uint64 ttl;
    }

    mapping (bytes32 => Record) records;
    mapping (address => mapping(address => bool)) operators;

    // Permits modifications only by the owner of the specified node.
    modifier authorised(bytes32 node) {
        address the_owner = records[node].owner;
        require(the_owner == msg.sender || operators[the_owner][msg.sender]);
        _;
    }

    /**
     * @dev Constructs a new ENS registrar.
     */
    constructor() {
        records[0x0].owner = msg.sender;
    }

    /**
     * @dev Sets the record for a node.
     * @param node The node to update.
     * @param the_owner The address of the new owner.
     * @param the_resolver The address of the resolver.
     * @param the_ttl The TTL in seconds.
     */
    function setRecord(bytes32 node, address the_owner, address the_resolver, uint64 the_ttl) external {
        setOwner(node, the_owner);
        _setResolverAndTTL(node, the_resolver, the_ttl);
    }

    /**
     * @dev Sets the record for a subnode.
     * @param node The parent node.
     * @param label The hash of the label specifying the subnode.
     * @param the_owner The address of the new owner.
     * @param the_resolver The address of the resolver.
     * @param the_ttl The TTL in seconds.
     */
    function setSubnodeRecord(bytes32 node, bytes32 label, address the_owner, address the_resolver, uint64 the_ttl) external {
        bytes32 subnode = setSubnodeOwner(node, label, the_owner);
        _setResolverAndTTL(subnode, the_resolver, the_ttl);
    }

    /**
     * @dev Transfers ownership of a node to a new address. May only be called by the current owner of the node.
     * @param node The node to transfer ownership of.
     * @param the_owner The address of the new owner.
     */
    function setOwner(bytes32 node, address the_owner) public authorised(node) {
        _setOwner(node, the_owner);
        emit Transfer(node, the_owner);
    }

    /**
     * @dev Transfers ownership of a subnode keccak256(node, label) to a new address. May only be called by the owner of the parent node.
     * @param node The parent node.
     * @param label The hash of the label specifying the subnode.
     * @param the_owner The address of the new owner.
     */
    function setSubnodeOwner(bytes32 node, bytes32 label, address the_owner) public authorised(node) returns(bytes32) {
        bytes32 subnode = keccak256(abi.encodePacked(node, label));
        _setOwner(subnode, the_owner);
        emit NewOwner(node, label, the_owner);
        return subnode;
    }

    /**
     * @dev Sets the resolver address for the specified node.
     * @param node The node to update.
     * @param the_resolver The address of the resolver.
     */
    function setResolver(bytes32 node, address the_resolver) public authorised(node) {
        emit NewResolver(node, the_resolver);
        records[node].resolver = the_resolver;
    }

    /**
     * @dev Sets the TTL for the specified node.
     * @param node The node to update.
     * @param the_ttl The TTL in seconds.
     */
    function setTTL(bytes32 node, uint64 the_ttl) public authorised(node) {
        emit NewTTL(node, the_ttl);
        records[node].ttl = the_ttl;
    }

    /**
     * @dev Enable or disable approval for a third party ("operator") to manage
     *  all of `msg.sender`'s ENS records. Emits the ApprovalForAll event.
     * @param operator Address to add to the set of authorized operators.
     * @param approved True if the operator is approved, false to revoke approval.
     */
    function setApprovalForAll(address operator, bool approved) external {
        operators[msg.sender][operator] = approved;
        emit ApprovalForAll(msg.sender, operator, approved);
    }

    /**
     * @dev Returns the keccak256 hash of a label.
     * @param label The specified label in string form.
     * @return keccak256 hash of the label.
     * @dev labelHash('eth') = 0x4f5b812789fc606be1b3b16908db13fc7a9adf7ca72641f84d75b47069d3d7f0
     */
    function labelHash(
        string calldata label
    ) external pure returns (bytes32) {
        return keccak256(abi.encodePacked(label));
    }

    /**
     * @dev Returns the name hash of (node, label).
     * @param node The specified namehash of node.     
     * @param label The specified labelhash of label.
     * @return namehash of the (node, label).
     * @dev nameHash(0x0000000000000000000000000000000000000000000000000000000000000000, 0x4f5b812789fc606be1b3b16908db13fc7a9adf7ca72641f84d75b47069d3d7f0) = 0x93cdeb708b7545dc668eb9280176169d1c33cfd8ed6f04690a0bcc88a93fc4ae
     */
    function nameHash(
        bytes32 node,
        bytes32 label
    ) external pure returns (bytes32) {
        return keccak256(abi.encodePacked(node, label));
    }

    /**
     * @dev Returns the address that owns the specified node.
     * @param node The specified node.
     * @return address of the owner.
     */
    function owner(bytes32 node) public view returns (address) {
        address addr = records[node].owner;
        if (addr == address(this)) {
            return address(0x0);
        }

        return addr;
    }

    /**
     * @dev Returns the address of the resolver for the specified node.
     * @param node The specified node.
     * @return address of the resolver.
     */
    function resolver(bytes32 node) public view returns (address) {
        return records[node].resolver;
    }

    /**
     * @dev Returns the TTL of a node, and any records associated with it.
     * @param node The specified node.
     * @return ttl of the node.
     */
    function ttl(bytes32 node) public view returns (uint64) {
        return records[node].ttl;
    }

    /**
     * @dev Returns whether a record has been imported to the registry.
     * @param node The specified node.
     * @return Bool if record exists
     */
    function recordExists(bytes32 node) public view returns (bool) {
        return records[node].owner != address(0x0);
    }

    /**
     * @dev Query if an address is an authorized operator for another address.
     * @param the_owner The address that owns the records.
     * @param operator The address that acts on behalf of the owner.
     * @return True if `operator` is an approved operator for `owner`, false otherwise.
     */
    function isApprovedForAll(address the_owner, address operator) external view returns (bool) {
        return operators[the_owner][operator];
    }

    function _setOwner(bytes32 node, address the_owner) internal {
        records[node].owner = the_owner;
    }

    function _setResolverAndTTL(bytes32 node, address the_resolver, uint64 the_ttl) internal {
        if(the_resolver != records[node].resolver) {
            records[node].resolver = the_resolver;
            emit NewResolver(node, the_resolver);
        }

        if(the_ttl != records[node].ttl) {
            records[node].ttl = the_ttl;
            emit NewTTL(node, the_ttl);
        }
    }
}