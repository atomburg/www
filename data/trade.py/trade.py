#!/bin/python3 

import sys,json,math
import okx.Trade as Trade
import okx.Account as Account

# API 初始化
apikey = sys.argv[1]
secretkey = sys.argv[2]
passphrase = sys.argv[3]

INSTID = sys.argv[4].upper()
INSTPX = sys.argv[5]
RATIO = 0.75
DIR=bool(int(sys.argv[6])) #1, 买入开多; 0, 卖出开空
SL_PERCENT=float(sys.argv[7])
TP_PERCENT=float(sys.argv[8])

side = 'buy' if DIR else 'sell'
posSide = 'long' if DIR else 'short'
slPx = "%0.20f"%(float(INSTPX)*(1.0 + (-1.0 if DIR else 1.0)*SL_PERCENT))
#print(slPx)
tpPx = "%0.20f"%(float(INSTPX)*(1.0 + (1.0 if DIR else -1.0)*TP_PERCENT))
#print(tpPx)

flag = "0"  # 实盘: 0, 模拟盘: 1
tradeAPI = Trade.TradeAPI(apikey, secretkey, passphrase, False, flag, debug=False)
accountAPI = Account.AccountAPI(apikey, secretkey, passphrase, False, flag, debug=False)

# 获取最大可买卖/开仓数量
result = accountAPI.get_instruments(instType="SWAP", instId=INSTID)
print(result)
lotSz = float(result['data'][0]['lotSz'])
lotSz_len = len(result['data'][0]['lotSz'])
ctVal = float(result['data'][0]['ctVal'])

# 获取最大可买卖/开仓数量
result = accountAPI.get_max_order_size(
    instId=INSTID,
    px=INSTPX,
    tdMode="isolated"
)
print(result)

maxBuy=float(result['data'][0]['maxBuy'])
#sz=str((int(maxBuy*ctVal*RATIO)/int(lotSz*ctVal)))
#print(sz)
#sz=str(int(int(maxBuy*ctVal*RATIO)/int(lotSz*ctVal)))
#print(sz)
maxBuy=float(result['data'][0]['maxBuy'])
sz=math.floor(maxBuy*RATIO/lotSz)*lotSz
if lotSz > 1:
    sz=round(sz) 
else:
    sz=round(sz, lotSz_len) 


#exit(1)

attachAlgoOrds= [{
	#"attachAlgoClOrdId": "attachAlgoClOrdId",
	"slTriggerPxType": "last",
	"slTriggerPx": slPx,
	"slOrdPx": slPx,
	"tpTriggerPxType": "last",
	"tpTriggerPx": tpPx,
	"tpOrdPx": tpPx 
}]

# 单向止盈止损
result = tradeAPI.place_algo_order_fix(
    instId=INSTID,
    tdMode="isolated",
    side=side,
    posSide=posSide,
    ordType="trigger",
    sz=maxBuy,
    tag="tag",
    #algoClOrdId="algoClientOrderId",
    triggerPxType="last",
    triggerPx=INSTPX,
    orderPx=INSTPX, #"-1",
    attachAlgoOrds=attachAlgoOrds
)
print(result['code'], result['data'][0]['algoId'])


print(result)
