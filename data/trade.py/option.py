#!/bin/python3 

import sys,json,math
import okx.Trade as Trade
import okx.Account as Account

# API 初始化
apikey = sys.argv[1]
secretkey = sys.argv[2]
passphrase = sys.argv[3]
flag = "0"  # 实盘: 0, 模拟盘: 1
tradeAPI = Trade.TradeAPI(apikey, secretkey, passphrase, False, flag, debug=False)
accountAPI = Account.AccountAPI(apikey, secretkey, passphrase, False, flag, debug=False)

INSTID = sys.argv[4].upper()
INSTPX = sys.argv[5]
SL_PERCENT=float(sys.argv[6])
TP_PERCENT=float(sys.argv[7])

FEE_RATE=0.0005
RATIO = 0.5

# 获取杠杆倍数
result = accountAPI.get_leverage(
    instId=INSTID,
    mgnMode="isolated"
)
print(result)
lever = result['data'][0]['lever']
FEE_PERCENT=FEE_RATE*float(lever)

# 获取单手lotSz
result = accountAPI.get_instruments(instType="SWAP", instId=INSTID)
print(result)

lotSz = result['data'][0]['lotSz']
lotSz_len = len(lotSz)
lotSz = float(lotSz)

# 获取最大可买卖/开仓数量
result = accountAPI.get_max_order_size(
    instId=INSTID,
    px=INSTPX,
    tdMode="isolated"
)
print(result)
maxBuy=float(result['data'][0]['maxBuy'])
sz=math.floor(maxBuy*RATIO/lotSz)*lotSz
if lotSz > 1:
    sz=round(sz) 
else:
    sz=round(sz, lotSz_len) 

print(sz)

def place(DIR):
    #DIR=bool(int(sys.argv[6])) #1, 买入开多; 0, 卖出开空
    side = 'buy' if DIR else 'sell'
    posSide = 'long' if DIR else 'short'
    slPx = "%0.20f"%(float(INSTPX)*(1.0 + (-1.0 if DIR else 1.0)*(SL_PERCENT)))
    print(slPx)
    tpPx = "%0.20f"%(float(INSTPX)*(1.0 + (1.0 if DIR else -1.0)*(TP_PERCENT+2*FEE_PERCENT)))
    print(tpPx)

    attachAlgoOrds= [{
        "slTriggerPxType": "last",
        "slTriggerPx": slPx,
        "slOrdPx": slPx,
        "tpTriggerPxType": "last",
        "tpTriggerPx": tpPx,
        "tpOrdPx": tpPx 
    }]

    # 单向止盈止损
    result = tradeAPI.place_algo_order_fix(
        instId=INSTID,
        tdMode="isolated",
        side=side,
        posSide=posSide,
        ordType="trigger",
        sz=sz,
        #algoClOrdId="algoClientOrderId",
        triggerPxType='last',
        triggerPx=INSTPX,
        orderPx=INSTPX,
        attachAlgoOrds=attachAlgoOrds
    )
    print(result['code'], result['data'][0]['algoId'])

    if result['code'] != 0:
        print(result)


place(True)
place(False)
