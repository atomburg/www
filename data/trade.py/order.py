#!/bin/python3 

import sys,json,math
import okx.Trade as Trade
import okx.Account as Account

# API 初始化
apikey = sys.argv[1]
secretkey = sys.argv[2]
passphrase = sys.argv[3]
flag = "0"  # 实盘: 0, 模拟盘: 1
tradeAPI = Trade.TradeAPI(apikey, secretkey, passphrase, False, flag, debug=False)
accountAPI = Account.AccountAPI(apikey, secretkey, passphrase, False, flag, debug=False)

INSTID = sys.argv[4]

# 查询所有未成交订单
result = tradeAPI.get_order_list(
    instType="SWAP",
    instId=INSTID
)
#print(result)

# 按ordId撤单
cancel_orders_with_orderId = [ ]

for ord in result['data']:
    cancel_orders_with_orderId.append( {"instId": INSTID, "ordId": ord['ordId']})

if len(cancel_orders_with_orderId) > 1:
    print(cancel_orders_with_orderId)
    result = tradeAPI.cancel_multiple_orders(cancel_orders_with_orderId)
    print(result)

# 查询所有未触发的单向止盈止损策略订单
result = tradeAPI.order_algos_list(
            ordType="trigger"
            )

algo_orders = []

for ord in result['data']:
    algo_orders.append( {"instId": INSTID, "algoId": ord['algoId']})

if len(algo_orders) > 1:
    print(algo_orders)
    result = tradeAPI.cancel_algo_order(algo_orders)
    print(result)

